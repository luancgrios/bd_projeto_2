#rodar com python 3.6
import requests

def create_database():
    database = input("Diga o nome da base de dados:\n")
    r = requests.post('http://localhost:8086/query', data = {'q':'CREATE DATABASE ' + str(database)})
    if not r.ok:
        print("Ocorreu algum erro, codigo da resposta: " + str(r.status_code) + " " + str(r.reason))
    else:
        print("Resposta: " + str(r.status_code) + " " + str(r.reason))

def write_line():
    database = input("*Escolha a base de dados:\n")
    measurement = input("*Diga qual a medida:\n")
    tags = input("Diga quais as tags e seus valores, separados por virgula (Ex: host=01,locale=BR):\n")
    if tags != '':
        tags = "," + tags
    fields = input("*Diga quais os campos e seus valores, separados por virgula (Ex: valor1=01,valor2='Val2'):\n")
    while fields == '':
        fields = input("*Por favor, informe pelo menos um campo (Ex: valor=0.1): ")
    time = input("Informe o tempo (em UTC):\n")
    r = requests.post('http://localhost:8086/write?db=' + str(database), data = str(measurement) + str(tags) + " " + str(fields) + " " + str(time))
    if not r.ok:
        print("Ocorreu algum erro, codigo da resposta: " + str(r.status_code) + " " + str(r.reason))
        print(r.content)
    else:
        print("Resposta: " + str(r.status_code) + " " + str(r.reason))

action = input("Escolha sua acao (create_database ou write_line):\n")
if action == "create_database":
    create_database()
elif action == "write_line":
    write_line()
