#rodar com python 3.6
import requests
import json
import threading
import time
import matplotlib.pyplot as plt


xdata = []
ydata = []

plt.show()

axes = plt.gca()
axes.set_title('Monitoramento de Uso')
axes.set_xlim(0,150)
axes.set_ylim(0,100)
line1, = axes.plot(xdata, ydata, color='#6c4763', label='CPU')
line2, = axes.plot(xdata, ydata, color='#e92aa2', label='Disco')
line3, = axes.plot(xdata, ydata, color='#a52a2a', label='Memoria')
axes.legend()

def task1():
    query = "SELECT MEAN(value) FROM cpu_usage GROUP BY time(200ms) fill(none) ORDER BY time DESC LIMIT 150"
    r = requests.post('http://localhost:8086/query?db=info', data = {'q':query})
    if r.ok:
        cpu_plot = []
        r_parsed = json.loads(r.text)
        values = r_parsed['results'][0]['series'][0]['values']
        for value in values:
            cpu_plot.append(round(value[1],2))
    else:
        print(r.content)
        exit(0)
    query = "SELECT MEAN(value) FROM mem_usage GROUP BY time(200ms) fill(none) ORDER BY time DESC LIMIT 150"
    r = requests.post('http://localhost:8086/query?db=info', data = {'q':query})
    if r.ok:
        mem_plot = []
        r_parsed = json.loads(r.text)
        values = r_parsed['results'][0]['series'][0]['values']
        for value in values:
            mem_plot.append(round(value[1],2))
    else:
        print(r.content)
        exit(0)
    query = "SELECT MEAN(value) FROM dsk_usage GROUP BY time(200ms) fill(none) ORDER BY time DESC LIMIT 150"
    r = requests.post('http://localhost:8086/query?db=info', data = {'q':query})
    if r.ok:
        dsk_plot = []
        r_parsed = json.loads(r.text)
        values = r_parsed['results'][0]['series'][0]['values']
        for value in values:
            dsk_plot.append(round(value[1],2))
    else:
        print(r.content)
        exit(0)
    cpu_plot.reverse()
    mem_plot.reverse()
    dsk_plot.reverse()
    xdata = range(150)
    ydata = cpu_plot
    line1.set_xdata(xdata)
    line1.set_ydata(ydata)
    ydata = dsk_plot
    line2.set_xdata(xdata)
    line2.set_ydata(ydata)
    ydata = mem_plot
    line3.set_xdata(xdata)
    line3.set_ydata(ydata)
    plt.draw()
    plt.pause(1e-17)
    threading.Timer(0.2,task1).start()

task1()

