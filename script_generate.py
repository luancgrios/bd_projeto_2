#rodar com python 3.6

import psutil
import threading
import time
import requests

count = 0
tempo = 0
cpu_usage = []
mem_usage = []
dsk_usage = []

def insert(vector_cpu,vector_mem,vector_dsk):
    data_string = ""
    for item in vector_cpu:
        data_string += 'cpu_usage value=' + str(item[0]) + ' ' + str(item[1]) + '\n'
    r = requests.post('http://localhost:8086/write?db=info', data = data_string)
    if not r.ok:
        print(r.content)
        exit(0)
    for item in vector_mem:
        data_string += 'mem_usage value=' + str(item[0]) + ' ' + str(item[1]) + '\n'
    r = requests.post('http://localhost:8086/write?db=info', data = data_string)
    if not r.ok:
        print(r.content)
        exit(0)
    for item in vector_dsk:
        data_string += 'dsk_usage value=' + str(item[0]) + ' ' + str(item[1]) + '\n'
    r = requests.post('http://localhost:8086/write?db=info', data = data_string)
    if not r.ok:
        print(r.content)
        exit(0)

def task():
    global count
    global time
    tempo = int(time.time()*1000000000)
    if count == 4:
        mem_usage.append([psutil.virtual_memory().percent,tempo])
        dsk_usage.append([psutil.disk_usage('/').percent,tempo])
        count = 0
        insert(cpu_usage,mem_usage,dsk_usage)
    cpu_usage.append([psutil.cpu_percent(),tempo])
    threading.Timer(0.05,task).start()
    count += 1

task()
